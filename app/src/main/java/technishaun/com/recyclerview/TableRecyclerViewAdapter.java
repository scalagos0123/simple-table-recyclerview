package technishaun.com.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TableRecyclerViewAdapter extends RecyclerView.Adapter<TableRecyclerViewAdapter.DataHolder> {

    /**
     * Reference kung saan naka-save column data mo
     */
    private List<Row> mDataset;

    /**
     * Reference lang to sa application.
     * Kailangan ni LayoutInflater ng application context para magamit mo
     * yung layout file na ginawa mo.
     */
    private Context mApplicationContext;

    /**
     * Default constructor para ma-set lahat
     * @param applicationContext application context (kunin mo na lang sa activity, then pasa mo dito)
     * @param dataset yung rows mo na may data nang naka-set sa columns mo (column1, column2, etc...)
     */
    public TableRecyclerViewAdapter(Context applicationContext, List<Row> dataset) {
        this.mDataset = dataset;
        this.mApplicationContext = applicationContext;
    }

    /**
     * Dito nyo isa-start yung layout file nyo, para malagyan yung mga column text dun sa layout file
     * @param viewGroup
     * @param i
     * @return
     */
    @NonNull
    @Override
    public DataHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Inflate mo muna yung layout file using LayoutInflater
        LayoutInflater inflater = LayoutInflater.from(mApplicationContext);
        View inflatedLayoutFile = inflater.inflate(R.layout.recyclerview_row, viewGroup, false);

        // pasa mo lang sa DataHolder. Need natin yung inflated layout
        return new DataHolder(inflatedLayoutFile);
    }

    /**
     * Dito nyo ikakabit yung mga column data.
     * @param dataHolder yung class na nagho-hold ng mga text sa layout file natin
     * @param i current position to na ina-access ng method. Eto gagamitin nyo para makuha yung row nyo sa dataset sa taas
     */
    @Override
    public void onBindViewHolder(@NonNull DataHolder dataHolder, int i) {
        dataHolder.bindData(
                mDataset.get(i).getColumn1(),
                mDataset.get(i).getColumn2(),
                mDataset.get(i).getColumn3()
        );
    }

    @Override
    public int getItemCount() {
        // by default, kunin mo lang lagi yung total na laman ng dataset mo. Oks na tong function na to in this case.
        return mDataset.size();
    }

    public class DataHolder extends RecyclerView.ViewHolder {
        // references for column text data
        private TextView column1Text, column2Text, column3Text;

        // reference sa layout file
        private View inflatedLayout;

        public DataHolder(View inflatedLayout) {
            super(inflatedLayout);
            this.inflatedLayout = inflatedLayout;

            // pag-start ng textviews
            column1Text = inflatedLayout.findViewById(R.id.txtv_col1);
            column2Text = inflatedLayout.findViewById(R.id.txtv_col2);
            column3Text = inflatedLayout.findViewById(R.id.txtv_col3);
        }

        /**
         * Dito nyo na ise-set yung text ng column data
         * @param column1
         * @param column2
         * @param column3
         */
        public void bindData(String column1, String column2, String column3) {
            column1Text.setText(column1);
            column2Text.setText(column2);
            column3Text.setText(column3);
        }
    }
}
