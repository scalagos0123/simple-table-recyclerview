package technishaun.com.recyclerview;

/**
 * Class para malagay nyo yung dataset nyo, per column
 */
public class Row {
    private String column1, column2, column3;

    public Row(String column1Val, String column2Val, String column3Val) {
        this.column1 = column1Val;
        this.column2 = column2Val;
        this.column3 = column3Val;
    }

    public String getColumn1() {
        return column1;
    }

    public void setColumn1(String column1) {
        this.column1 = column1;
    }

    public String getColumn2() {
        return column2;
    }

    public void setColumn2(String column2) {
        this.column2 = column2;
    }

    public String getColumn3() {
        return column3;
    }

    public void setColumn3(String column3) {
        this.column3 = column3;
    }
}
