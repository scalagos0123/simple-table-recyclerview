package technishaun.com.recyclerview;

import android.os.Bundle;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Row> mDataset;
    private RecyclerView mRecyclerViewFromActivity;
    private TableRecyclerViewAdapter mDatasetView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initDummyData();
        initRecyclerViewSettings();
    }

    /**
     * Lagyan ko lang ng dummy data na puro columns
     */
    private void initDummyData() {
        mDataset = new ArrayList<>();

        for (int i = 0; i < 30; i++) {
            Row currentRow = new Row(
                    "Column " + i,
                    "Column " + (i + 1),
                    "Column " + (i + 2)
            );

            mDataset.add(currentRow);
        }
    }

    /**
     * Initialize ng recyclerview ng holder ng dataset mo, and additional settings.
     */
    private void initRecyclerViewSettings() {
        // start ng table dataset mo
        mDatasetView = new TableRecyclerViewAdapter(this.getApplicationContext(), mDataset);

        // kunin natin yung recyclerview from your activity
        this.mRecyclerViewFromActivity = findViewById(R.id.recyclerview);

        // kabit yung table dataset mo sa adapter ng recyclerview. Eto yung part na para makita yung dataset mo
        this.mRecyclerViewFromActivity.setAdapter(mDatasetView);

        // important to set. gagawin lang natin na vertically-scrollable list yung dataset mo
        this.mRecyclerViewFromActivity.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

    }
}
